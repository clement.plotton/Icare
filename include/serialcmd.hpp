/*
 * image.h
 *
 *  Created on: 5 mars 2016
 *      Author: clement
 */

#ifndef INCLUDE_IMAGE_H_
#define INCLUDE_IMAGE_H_

using namespace std;

bool openSerial();

void closeSerial();

bool commandRight();

bool commandLeft();

bool commandBackRight();

bool commandBackLeft();

bool commandStop();

bool commandForward();

bool commandBackward();

#endif /* INCLUDE_IMAGE_H_ */
