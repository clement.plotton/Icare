/*
 * calcul.h
 *
 *  Created on: 5 mars 2016
 *      Author: clement
 */

#ifndef INCLUDE_CALCUL_H_
#define INCLUDE_CALCUL_H_

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <assert.h>

#define NB_BORDS_POTENTIELS 5
#define ECART_MAX_ENTRE_BORD_G_ET_D 4
#define ECART_MIN_ENTRE_BORD_G_ET_D 2

class ligneParcours{
public:

	void initialiser();
	void set_taille(int);
	int get_taille();

	void inc_ligneParcoursVersDroite(int);
	int get_ligneParcoursVersDroite(int);

	void inc_ligneParcoursVersGauche(int);
	int get_ligneParcoursVersGauche(int);

	void set_ligne(int, int);
	int get_ligne(int);

	void display_ligne();
	void display_indices_droit();

	short get_indice_bord_gauche();
	short get_indice_bord_droit();

	void calcul(short taille);

	ligneParcours();
	~ligneParcours();

	short indice_bord_gauche;
	short indice_bord_droit;
private:
	short* ligneParcoursVersDroite;
	short* ligneParcoursVersGauche;
	short* indices_bord_gauche;
	short* indices_bord_droit;
	short* solutions;

	unsigned char* ligne;
	int taille;
};

ligneParcours* ligneParcours_initialiser(int taille);

#endif /* INCLUDE_CALCUL_H_ */
