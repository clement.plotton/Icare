/*
 * image.h
 *
 *  Created on: 5 mars 2016
 *      Author: clement
 */

#ifndef INCLUDE_IMAGE_H_
#define INCLUDE_IMAGE_H_

#include <opencv2/opencv.hpp>
#include <cstdlib>
#include <iostream>
#include <cmath>

#include <calcul.h>

#define NB_LIGNES_TRAITE 2

class image{
public :
	image();
	~image();
	int get_height();
	int get_width();
	int get_pix(int ligne, int col);
	void set_pix(int ligne, int col, int val);
	void load(char* path);
	void load_from_cam(cv::Mat);
	void display();

	short get_no_ligne(short i);
	short get_bord_gauche(short i);
	short get_bord_droit(short i);
	void set_no_ligne(short val);
	void set_bords(short, short);

	void traitement_ligne(int ligne, int taille);

	ligneParcours* ligne;
	cv::Mat frame;
private :
	short* detection_ligne;
	short indice_detection_ligne;

};


image* image_create();

void image_process(char*);








#endif /* INCLUDE_IMAGE_H_ */
