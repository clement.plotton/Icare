#include <image.h>

using namespace cv;
using namespace std;



image::image(){
	Mat frame;
	detection_ligne = NULL;
	//ligneParcours* ligne = NULL;
	indice_detection_ligne = 0;

}

image::~image(){
	delete [] detection_ligne;
}

int image::get_height(){
	return this->frame.rows;
}

int image::get_width(){
	return this->frame.cols;
}

int image::get_pix(int ligne, int col){
	assert(ligne < this->frame.rows && col < this->frame.cols);
	return this->frame.at<uchar>(ligne, col);
}

void image::set_pix(int ligne, int col, int val){
	assert(ligne < this->frame.rows && col < this->frame.cols);
	assert(val >= 0 && val <= 255);
	this->frame.at<uchar>(ligne, col) = val;
}



void image::load(char* path){
	this->frame = imread(path, CV_LOAD_IMAGE_GRAYSCALE);
	this->ligne = ligneParcours_initialiser(this->get_width());

	this->detection_ligne = new short [NB_LIGNES_TRAITE * 3];
	for(int i=0; i< (NB_LIGNES_TRAITE *3);i++){
		detection_ligne[i] = 0;
	}
}

void image::load_from_cam(Mat frame){
	this->frame = frame;


	this->detection_ligne = new short [NB_LIGNES_TRAITE * 3];
	for(int i=0; i< (NB_LIGNES_TRAITE *3);i++){
		detection_ligne[i] = 0;
	}
}

//Gestion du tableau detection_ligne
short image::get_no_ligne(short i){
	assert(i<NB_LIGNES_TRAITE);
	return detection_ligne[i* 3];
}
short image::get_bord_gauche(short i){
	assert(i<NB_LIGNES_TRAITE);
	return detection_ligne[i* 3 + 1 ];
}
short image::get_bord_droit(short i){
	assert(i<NB_LIGNES_TRAITE);
	return detection_ligne[i* 3 + 2 ];
}

void image::set_no_ligne(short val){
	assert(indice_detection_ligne<NB_LIGNES_TRAITE);
	detection_ligne[indice_detection_ligne *3] = val;
}
void image::set_bords(short bord_gauche, short bord_droit){
	assert(indice_detection_ligne<NB_LIGNES_TRAITE);
	detection_ligne[indice_detection_ligne*3 +1] = bord_gauche;
	detection_ligne[indice_detection_ligne * 3 + 2] = bord_droit;
	indice_detection_ligne++;
}


void image::display(){
	imshow("Mon_image", this->frame);
	waitKey();
}

void image::traitement_ligne(int ligne, int taille){
	//Initialisation objet ligne
	this->ligne = ligneParcours_initialiser(this->get_width());

	for (int j = taille; j < this->get_width(); j++) {
		int max = 0;
		for (int k = 1; k <= taille; k++) {
			int diff = abs(this->get_pix(ligne,j)- this->get_pix(ligne, j - k));
			if (diff > max)
				max = diff;
		}
		if (max < 5)
			this->ligne->set_ligne(j,255);
		else
			this->ligne->set_ligne(j,0);
	}

	for(int j=taille;j<(this->get_width() - taille) ;j++){
		//Parcours des colonnes
		for(int k=1;k<=taille;k++){
			//Pour chaque pixel sur la colonne, on cherche à savoir
			//si c'est le bord droit ou gauche de la ligne
			if(this->ligne->get_ligne(j+k) == 255)
				this->ligne->inc_ligneParcoursVersDroite(j);
			if(this->ligne->get_ligne(j-k) == 0)
				this->ligne->inc_ligneParcoursVersDroite(j);
			if(this->ligne->get_ligne(this->get_width()-(j+1)+k)==0)
				this->ligne->inc_ligneParcoursVersGauche(this->get_width()-j);
			if(this->ligne->get_ligne(this->get_width()-(j+1)-k)==255)
				this->ligne->inc_ligneParcoursVersGauche(this->get_width()-j);
		}
	}

	this->ligne->calcul(taille);

	this->set_bords(this->ligne->get_indice_bord_gauche(),this->ligne->get_indice_bord_droit());

	delete this->ligne;
}

image* image_create(){
	image* mon_image = new image();
	return mon_image;
}

void image_process(Mat frame,int* alpha, int* delta){
	image* image = image_create();
	image->load_from_cam(frame);
	//image->load(path);

	short inter_ligne = floor(image->get_height()/(NB_LIGNES_TRAITE+1));

	for(int i=1;i<=NB_LIGNES_TRAITE;i++){
		image->set_no_ligne(i * inter_ligne);
		image->traitement_ligne(i* inter_ligne,24);
	}

	Point g1(image->get_bord_gauche(0),image->get_no_ligne(0));
	Point g2(image->get_bord_gauche(1),image->get_no_ligne(1));
	Point d1(image->get_bord_droit(0),image->get_no_ligne(0));
	Point d2(image->get_bord_droit(1),image->get_no_ligne(1));


	line(image->frame,g1,g2,CV_RGB(255, 0, 0),5);
	line(image->frame,d1,d2,CV_RGB(255, 0, 0),5);

	image->display();

	for(int i=0;i<NB_LIGNES_TRAITE;i++){
		//printf("[%d] Bord Gauche : %d || Bord droit : %d\n",image->get_no_ligne(i),image->get_bord_gauche(i),image->get_bord_droit(i));
	}

	delete image;
}
