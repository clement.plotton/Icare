#include "serialib.hpp"

using namespace std;
serialib sl;
char lastCommand;
bool openSerial()
{
    bool res = false;
    
    if (sl.Open("/dev/ttyUSB0", 9600) != 1) //@check nom du port
        cout << "Failed to open serial port!" << endl;
    else
        cout << "Serial port successfully opened." << endl;
        res = true;
    
    return res;
}

void closeSerial()
{
    cout << "Closing current port..." << endl;
    sl.Close();
    cout << "Port successfully closed." << endl;
}

bool commandRight()
{
    bool res;
    
    if (sl.WriteChar('R') == 1) // Right
    {
        cout << "A droite toute !" << endl;
        res = true;
    }
    
    lastCommand = 'R';
    return res;
}

bool commandLeft()
{
    bool res;
    
    if (sl.WriteChar('L') == 1) // Left
    {
        cout << "A gauche toute !" << endl;
        res = true;
    }
    
    lastCommand = 'L';
    return res;
}

bool commandBackRight()
{
    bool res;
    
    if (sl.WriteChar('D') == 1) // Right
    {
        cout << "Recule à droite !" << endl;
        res = true;
    }
    
    return res;
}

bool commandBackLeft()
{
    bool res;
    
    if (sl.WriteChar('G') == 1) // Left
    {
        cout << "Recule à gauche !" << endl;
        res = true;
    }
    
    return res;
}

bool commandStop()
{
    bool res = false;
    
    if (sl.WriteChar('S') == 1) // Stop
    {
        cout << "Halte !" << endl;
        res = true;
    }
    
    return res;
}

bool commandForward()
{
    bool res = false;
    
    if (sl.WriteChar('F') == 1) // Forward
    {
        cout << "Droit devant !" << endl;
        res = true;
    }

    lastCommand = 'F';
    return res;
}

bool commandBackward()
{
    bool res = false;
    
    if (sl.WriteChar('B') == 1) // Backward
    {
        cout << "En arrière !" << endl;
        res = true;
    }
    
    return res;
}
