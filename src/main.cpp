#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <serialcmd.hpp>

#include <image.h>
#include <calcul.h>
//#include <opencv.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;


VideoCapture cap("/media/data/Shared/Banshee.S03E04.720p.HDTV.x264-IMMERSE.mkv");
double start, stop;
Mat frame;

void usage (const char *s){
  std::cerr<<"Usage: "<<s<<" ims1 imd val"<<std::endl;
  exit(EXIT_FAILURE);
}

#define param 3
bool setup(VideoCapture &videocap){
	bool is_ok = false;
	is_ok = videocap.isOpened();
	return is_ok;
}
int cas(int delta,int alpha,int alpha_min,int rows,int cols){
	if(delta < cols/2 ){
		if(delta > cols/8){
			if(alpha < alpha_min){
				return 1;
			}
			else{
				return 2;
			}
		}
		if(delta <= cols/8){
			if(alpha < alpha_min){
				return 1;
			}
			else{
				return 2;
			}
		}
	}
	else{
		return 3;
	}
	return 0;
}

void lastcmd(){
	switch (lastCommand){
	case 'R' :
	  commandRight();
	  break;
	case 'L' :
	  commandLeft();
	  break;
	default :
	  commandBackward();
	  break;
	}
}
void undolastcmd(){
	switch (lastCommand){
	case 'R' :
	  commandLeft();
	  break;
	case 'L' :
	  commandRight();
	  break;
	default :
	  commandBackward();
	  break;
	}
}
void command(int alpha, int delta,int rows,int cols){
	if(delta > 0){
		if(alpha > 0){
			switch(cas(delta,alpha,20,rows,cols)){
			case 1:
				commandForward();
				break;
			case 2:
				commandRight();
				break;
			case 3:
				commandStop();
				undolastcmd();
				break;
			default:
				break;
			}
		}
		else{
			switch(cas(delta,-alpha,20,rows,cols)){
			case 1:
				commandForward();
				break;
			case 2:
				commandLeft();
				break;
			case 3:
				commandStop();
				undolastcmd();
				break;
			default:
				break;
			}
		}
	}
	else{
		if(alpha > 0){
			switch(cas(-delta,alpha,20,rows,cols)){
			case 1:
				commandForward();
				break;
			case 2:
				commandLeft();
				break;
			case 3:
				commandStop();
				undolastcmd();
				break;
			default:
				break;
			}
		}
		else{
			switch(cas(-delta,-alpha,20,rows,cols)){
			case 1:
				commandForward();
				break;
			case 2:
				commandLeft();
				break;
			case 3:
				commandStop();
				undolastcmd();
				break;
			default:
				break;
			}
		}
	}
	lastcmd();
	commandForward();
}
void loop(){
	start = (double) getTickCount();

	namedWindow("edges",1);

	cap >> frame; // get a new frame from camera
	//printf("[%d]tps : %f\n",i++,difftime(end,start));
	//if(waitKey(30) >= 0) break;
	int alpha,delta;
	// the camera will be deinitialized automatically in VideoCapture destructor
	if(frame.cols != 0 && frame.rows != 0){
		image_process(frame,&alpha,&delta);
	}
	stop = (double) getTickCount();
	cout << (stop - start) / getTickFrequency() << endl;
	command(alpha,delta,frame.rows,frame.cols);
}


int main( int argc, char* argv[] ){
	bool setup_ok = setup(cap);
	while(setup_ok){
		loop();
	}

return EXIT_SUCCESS;  
}
