#include <calcul.h>
#include <stdio.h>



ligneParcours* ligneParcours_initialiser(int taille){
	assert(taille > 0);
	ligneParcours* ligne = new ligneParcours();
	ligne->set_taille(taille);
	ligne->initialiser();
	return ligne;
}

ligneParcours::ligneParcours(){
	ligne = NULL;
	ligneParcoursVersDroite = NULL;
	ligneParcoursVersGauche = NULL;
	indices_bord_droit = NULL;
	indices_bord_gauche = NULL;
	solutions = NULL;
	indice_bord_gauche = -1;
	indice_bord_droit = -1;
	taille = 0;
}

ligneParcours::~ligneParcours(){
	//Destructeur
	delete [] ligneParcoursVersDroite;
	delete [] ligneParcoursVersGauche;
	delete [] ligne;
}

void ligneParcours::initialiser(){
	assert(this->taille > 0);
	this->ligneParcoursVersDroite = new short[this->taille];
	this->ligneParcoursVersGauche = new short[this->taille];
	this->ligne = new unsigned char[this->taille];
	for(int i; i< this->taille; i++){
		ligneParcoursVersDroite[i] = 0;
		ligneParcoursVersGauche[i] = 0;
		ligne[i] = 0;
	}
}

int ligneParcours::get_ligneParcoursVersDroite(int i){
	assert(i < this->taille && i>=0);
	return ligneParcoursVersDroite[i];
}

int ligneParcours::get_ligneParcoursVersGauche(int i){
	assert(i < this->taille && i>=0);
	return ligneParcoursVersGauche[i];
}

void ligneParcours::inc_ligneParcoursVersDroite(int i){
	assert(i < this->taille && i>=0);
	ligneParcoursVersDroite[i] += 1;
}

void ligneParcours::inc_ligneParcoursVersGauche(int i){
	assert(i < this->taille && i>=0);
	ligneParcoursVersGauche[i] += 1;
}

void ligneParcours::set_ligne(int i, int val){
	assert(i < this->taille && i>=0);
	this->ligne[i] = val;
}

int ligneParcours::get_ligne(int i){
	assert(i < this->taille && i>=0);
	return ligne[i];
}

void ligneParcours::display_ligne(){
	for (int i = 0; i< this->taille; i++){
		printf("[%d] %d \n", i, this->get_ligneParcoursVersGauche(i));
	}
}

void ligneParcours::display_indices_droit(){
	for (int i = 0;i<NB_BORDS_POTENTIELS;i++){
		printf("Droit %d : %d\n",i,indices_bord_droit[i]);
	}
}

int ligneParcours::get_taille(){
	assert(taille>0);
	return taille;
}

void ligneParcours::set_taille(int val){
	assert(val > 0);
	taille = val;
}

short ligneParcours::get_indice_bord_gauche(){
	return this->indice_bord_gauche;
}

short ligneParcours::get_indice_bord_droit(){
	return this->indice_bord_droit;
}

short* initialiser_tab(short taille){
	short * tab = new short [taille];
	for (int i=0; i<taille; i++)
		tab[i]=0;
	return tab;
}

short recherche_tab(short* tab,short elt,short taille,short domaine){
	for (int i=0;i<taille;i++){
		if(abs(tab[i] - elt) < domaine){
			return i;
		}
	}
	return -1;
}

bool test_match(short bord_gauche,short bord_droit,short taille){
	if(bord_droit - bord_gauche > 0 && bord_droit - bord_gauche < floor((ECART_MAX_ENTRE_BORD_G_ET_D - 1) * taille ) && bord_droit - bord_gauche > floor((ECART_MIN_ENTRE_BORD_G_ET_D - 1) * taille))
		return true;
	return false;
}

void ligneParcours::calcul(short taille){
	indices_bord_gauche = initialiser_tab(NB_BORDS_POTENTIELS +1);
	indices_bord_droit = initialiser_tab(NB_BORDS_POTENTIELS +1);

	//this->display_ligne();

	for(int i=0; i<this->taille; i++){
		if(get_ligneParcoursVersDroite(i) > floor(taille*1.5)){
			for(int k=0; k<NB_BORDS_POTENTIELS;k++){

				if(get_ligneParcoursVersDroite(i) > get_ligneParcoursVersDroite(indices_bord_gauche[k])){
					int indice_recherche = recherche_tab(indices_bord_gauche,i,NB_BORDS_POTENTIELS+1,2*taille);
					if(indice_recherche == -1){
						//printf("// %d : %d\n",i,get_ligneParcoursVersDroite(i));
						short tmp = indices_bord_gauche[k];
						indices_bord_gauche[k] = i;
						indices_bord_gauche[k+1] = tmp;
					}
					else{
						if(get_ligneParcoursVersDroite(i) > get_ligneParcoursVersDroite(indices_bord_gauche[indice_recherche]))
							indices_bord_gauche[indice_recherche] = i;
							for(int h = 0;h<indice_recherche;h++){
								if(get_ligneParcoursVersDroite(i) > get_ligneParcoursVersDroite(indices_bord_gauche[h])){
									indices_bord_gauche[indice_recherche] = indices_bord_gauche[h];
									indices_bord_gauche[h] = i;
									break;

								}
							}
					}
					break;
				}
			}
		}

		if(get_ligneParcoursVersGauche(i) > floor(taille*1.5)){
			for(int k=0; k<NB_BORDS_POTENTIELS;k++){
				if(get_ligneParcoursVersGauche(i) > get_ligneParcoursVersGauche(indices_bord_droit[k]) ){
					int indice_recherche = recherche_tab(indices_bord_droit,i,NB_BORDS_POTENTIELS+1,2*taille);
					if(indice_recherche == -1){
						short tmp = indices_bord_droit[k];
						indices_bord_droit[k] = i;
						indices_bord_droit[k+1] = tmp;
					}
					else{
						if(get_ligneParcoursVersGauche(i) > get_ligneParcoursVersGauche(indices_bord_droit[indice_recherche]))
							indices_bord_droit[indice_recherche] = i;
						for(int h = 0;h<indice_recherche;h++){
							if(get_ligneParcoursVersGauche(i) > get_ligneParcoursVersGauche(indices_bord_droit[h])){
								indices_bord_droit[indice_recherche] = indices_bord_droit[h];
								indices_bord_droit[h] = i;
								break;
							}
						}
					}
					break;
				}
			}
		}
	}

	//this->display_indices_droit();

	solutions = new short [NB_BORDS_POTENTIELS*NB_BORDS_POTENTIELS];
	for(int i=0; i<NB_BORDS_POTENTIELS;i++){
		for(int j=0; j<NB_BORDS_POTENTIELS;j++){
			solutions[i*NB_BORDS_POTENTIELS + j] = get_ligneParcoursVersDroite(indices_bord_gauche[i]) + get_ligneParcoursVersGauche(indices_bord_droit[j]);
		}
	}


	short bord_gauche = -1;
	short bord_droit = 0;
	for(int i=0; i<NB_BORDS_POTENTIELS;i++){
		for(int j=0; j<NB_BORDS_POTENTIELS;j++){
			if(bord_gauche != -1){
				if (solutions[i*NB_BORDS_POTENTIELS + j] > solutions[bord_gauche*NB_BORDS_POTENTIELS + bord_droit] && test_match(indices_bord_gauche[i],indices_bord_droit[j],taille)){
					bord_gauche = indices_bord_gauche[i];
					bord_droit = indices_bord_droit[j];
				}
			}
			else{
				if (solutions[i*NB_BORDS_POTENTIELS + j] > 3* taille && test_match(indices_bord_gauche[i],indices_bord_droit[j],taille)){
					bord_gauche = indices_bord_gauche[i];
					bord_droit = indices_bord_droit[j];
				}
			}
		}
	}

	if (bord_gauche != -1){
		indice_bord_gauche = bord_gauche - taille;
		indice_bord_droit = bord_droit;
	}
}



