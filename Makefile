CC = g++

OPENCV_CFLAGS = `pkg-config --cflags opencv`
OPENCV_LIBS = -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_features2d -lopencv_calib3d -lopencv_imgcodecs -lopencv_video -lopencv_videoio
CFLAGS = -Wall -O0 -g -Iinclude/ $(OPENCV_CFLAGS)
#CFLAGS = -Wall -g -Iinclude/ $(OPENCV_CFLAGS)
LDFLAGS = $(OPENCV_LIBS)

BINDIR = bin
BIN = prog

SRCDIR = src
SRC  = $(wildcard $(SRCDIR)/*.cpp)

OBJDIR = .obj
OBJ  = $(SRC:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

DEPDIR = .dep
DEP = $(SRC:$(SRCDIR)/%.cpp=$(DEPDIR)/%.d)

.PHONY: all clean mrproper archive

all : $(BIN)

$(BIN) : $(OBJ)
	$(CC) -o $(BINDIR)/$@ $(OBJ) $(LDFLAGS)

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp $(DEPDIR)/%.d
	$(CC) -c $(CFLAGS) $< -o $@

$(DEPDIR)/%.d : $(SRCDIR)/%.c
	@$(CC) -Iinclude/ -MT"$(<:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)" -MM -o $@ $< 

$(OBJ) : $(OBJDIR)

$(DEP) : $(DEPDIR)

$(BIN) : $(BINDIR)

$(DEPDIR) $(OBJDIR) $(BINDIR):
	-mkdir $@

clean :
	-rm -rf $(BINDIR) $(OBJDIR) $(DEPDIR)
	
mrproper : clean
	-rm -rf .project .cproject .settings '*~' #*

archive:
	-zip -9 -r traitement_image-`date +'%y-%m-%d-%Hh%M'`.zip . -x "*/\.*" -x "\.*" -x "bin/*" -x "*.zip" -x "*~"

-include $(DEP)
